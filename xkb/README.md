# XKB PinePhone Keyboard Information

### Xkb files from Undef

The |\£€~-=_+ section of the top row can be accessed using AltG + top row keys. This is set in the xkb settings which were upstreamed by Undef. This was added in version xkeyboard-conf version > 2.35.1

XKB Source Information -  
https://gitlab.com/mobian1/packages/xkeyboard-config and,  
https://gitlab.freedesktop.org/xkeyboard-config/xkeyboard-config/-/merge_requests/322

### Phosh Interfaces

Phosh (Mobian) Information -  
See the following: https://wiki.mobian-project.org/doku.php?id=ppaccessories

Phosh (Arch) - 
Update your `/usr/share/X11/xkb/*` to the files in https://codeberg.org/HazardChem/PinePhone_Keyboard/src/branch/main/xkb. If you don't want to change the files in `/usr/share/xkb/` you can create them in your home directory, see [here](https://xkbcommon.org/doc/current/md_doc_user_configuration.html).

Add the folllowing in `~/.config/kxkbrc`

```
[Layout]
LayoutList=us
Model=ppkb
```

### SXMO Wayland

SWMO/Sway Information -  
Since 31/05/22 Sway has the config included to set the keyboard correctly. Leaving exisiting files for records.

07/02/23 https://lists.sr.ht/~mil/sxmo-devel/patches/38774 proposed removing the ppkb configs as they are added by the device profile in pmOS. Check if these settings are included in your sway config file.

### X11 UI, includes dwm from SXMO

X11 based UIs -   
Add `10-pinephone-keyboard.conf` to the following directory `/etc/X11/xorg.conf.d/` and restart your X11 server. The config will apply when an input detected by xinput matches PinePhone Keyboard to only that input, others will be set to `us` layout.


### General Information from my research

General XKB Information -  
See the following for US layout issues, us(intl) works out of the box but the ' turns into an accent for letters:
https://wiki.archlinux.org/title/Xmodmap

Use the files located in rules and symbols directories to confirm that the ppkb settings for xkb are in your local files

Check the following locations:  
`/usr/share/X11/xkb/(rules or symbols)`  
Against the same named files in the directories that follow.

### Troubleshooting

There is a script located in this directory `ppkb_settings_test.sh` and pulls diagnostic information.

This can be checked by using the tool `diff` on the your local files against the above, using the script `ppkb_settings_test.sh`, or you can inspect the files by hand.