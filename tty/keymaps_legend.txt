Info for anyone trying to understand the en.map keymap

keymaps 0-2,4-5,8-9,12 <-Designates the key combos that will be accepted

1	2	3	4	5	6	7	8	<-Count from =
0	1	2	4	5	8	9	12	<-Keycode
base	shift	AltGr	Ctrl	S-Ctrl	Alt	S-Alt	Ctrl-Alt <-Key combo
